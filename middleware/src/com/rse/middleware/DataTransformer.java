package com.rse.middleware;

import java.util.ArrayList;

import abs.backend.java.lib.runtime.ABSObject;
import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSValue;
import abs.backend.java.lib.types.ABSBuiltInDataType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/*
 * ACKNOWLADGE: https://github.com/sir-muamua/ABSServer/blob/master/src/com/fmse/absserver/helper/DataTransformer.java
 */
public class DataTransformer {
    public static String convertAbsResponseToJSON(ABSValue obj)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        // Appending Data Key, as requirement of IFML
        return "{\"data\": ".concat(convertAbsDataToJSON(obj)).concat("}");
    }

    public static String convertAbsDataToJSON(ABSValue obj)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (obj == null) {
            return "null";
        } else if (obj instanceof ABS.StdLib.List_Nil) {
            return "[]";
        } else if (obj instanceof ABS.StdLib.List) {
            return convertAbsListToJSON((ABS.StdLib.List) obj);
        } else if (obj instanceof ABSBuiltInDataType) {
            return convertAbsBuiltInDataToJSON(obj);
        } else {
            return convertAbsObjectToJSON(obj);
        }
    }

    public static String convertAbsBuiltInDataToJSON(ABSValue obj) {
        String value = obj.toString();
        if (obj instanceof ABSString) {
            String tempValue = value.substring(1, value.length() - 1);
            tempValue = tempValue.replace("\\", "\\\\").replace("\"", "\\\"");
            tempValue = tempValue.replaceAll("(\r\n|\n|\r)", "\\\\n");
            return "\"".concat(tempValue).concat("\"");
        }
        return value;
    }

    public static String convertAbsListToJSON(ABS.StdLib.List<ABSValue> dataModels)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        String json = "";
        do {
            json = json.concat(convertAbsDataToJSON(ABS.StdLib.head_f.apply(dataModels)));
            dataModels = ABS.StdLib.tail_f.apply(dataModels);
            if (!(dataModels instanceof ABS.StdLib.List_Nil)) {
                json = json.concat(", ");
            }
        } while (!(dataModels instanceof ABS.StdLib.List_Nil));
        return "[".concat(json).concat("]");
    }

    public static String convertAbsObjectToJSON(ABSValue obj)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        ABSObject absobj = (ABSObject) obj;
        Class cls = absobj.getClass();
        List<String> fields = (List<String>) cls.getDeclaredMethod("getFieldNames").invoke(obj);
        if (fields.isEmpty()) {
            return "null";
        }
        int ii = 0;
        String json = "";
        for (String field : fields) {
            json = json.concat("\"" + field + "\"" + ": ");
            Method m = cls.getDeclaredMethod("getFieldValue", field.getClass());
            m.setAccessible(true);
            json = json.concat(convertAbsDataToJSON((ABSValue) m.invoke(obj, field)));
            ii++;
            if (ii < fields.size()) {
                json = json.concat(", ");
            }
        }
        return "{".concat(json).concat("}");
    }
}
